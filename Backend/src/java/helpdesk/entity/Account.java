/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import helpdesk.webservices.pojos.AccountModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "accounts")
public class Account extends BaseEntity {
    
    @Column(name = "user_name", nullable = false, unique = true)
    private String userName;
    
    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "password", nullable = false)
    private String password;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "address")
    private String address;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "account")
    private Administrator adminRole;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "account")
    private Contractor contractorRole;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "account")
    private Migrant migrantRole;
    
    @OneToOne(fetch = FetchType.EAGER, mappedBy = "account")
    private Volunteer volunteerRole;

    
    public Account() {
        
    }
    
    public Account(AccountModel accountModel) {
        this.setAddress(accountModel.getAddress());
        this.setEmail(accountModel.getEmail());
        this.setPassword(accountModel.getPassword());
        this.setPhone(accountModel.getPhone());
        this.setUserName(accountModel.getUserName());
        this.setFirstName(accountModel.getFirstName());
        this.setLastName(accountModel.getLastName());
    }
        
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Administrator getAdminRole() {
        return adminRole;
    }

    public Contractor getContractorRole() {
        return contractorRole;
    }

    public Migrant getMigrantRole() {
        return migrantRole;
    }

    public Volunteer getVolunteerRole() {
        return volunteerRole;
    }

    protected void setAdminRole(Administrator adminRole) {
        this.adminRole = adminRole;
    }

    protected void setContractorRole(Contractor contractorRole) {
        this.contractorRole = contractorRole;
    }

    protected void setMigrantRole(Migrant migrantRole) {
        this.migrantRole = migrantRole;
    }

    protected void setVolunteerRole(Volunteer volunteerRole) {
        this.volunteerRole = volunteerRole;
    }
}
