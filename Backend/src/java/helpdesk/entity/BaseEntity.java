/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import helpdesk.services.IdGenerator;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author Revan
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    
    @Id
    @Column(name = "id", unique = true, updatable = false)
    private final String id;
    
    
    public BaseEntity() {
        id = IdGenerator.generateId();
    }

    public String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BaseEntity)) {
            return false;
        }
        BaseEntity other = (BaseEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "helpdesk.entity.BaseEntity[ id=" + id + " ]";
    }
}
