/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import helpdesk.webservices.pojos.ContractorModel;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "contractors")
public class Contractor extends Role {
    
    @Column(name = "charge_type")
    private String chargeType;
    
    @Column(name = "hourly_rate")
    private Integer hourlyRate;
    
    @ElementCollection
    @CollectionTable(name = "contrator_proffesions",
            joinColumns = @JoinColumn(name="contractor_id"))
    private List<String> proffesions;
    
    public Contractor() {
        
    }
    
    public Contractor(ContractorModel contractorModel) {
        hourlyRate = contractorModel.getHourlyRate();
        chargeType = contractorModel.getChargeType();
        proffesions = (contractorModel.getServices() == null) ? new ArrayList<String>() : new ArrayList<String>(contractorModel.getServices());
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Integer hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public List<String> getProffesions() {
        return proffesions;
    }

    public void setProffesions(List<String> proffesions) {
        this.proffesions = proffesions;
    }
}
