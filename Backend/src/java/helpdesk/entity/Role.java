/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import helpdesk.webservices.pojos.AccountModel;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

/**
 *
 * @author Revan
 */
@MappedSuperclass
public abstract class Role extends BaseEntity {
    
    @OneToOne(optional = false)
    @JoinColumn(name = "account", unique = true, nullable = false, updatable = false)
    private Account account;

    public Role() {
        
    }
    
    public Role(AccountModel accountModel) {
        
    }
    
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
}
