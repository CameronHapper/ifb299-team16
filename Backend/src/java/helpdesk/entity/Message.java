/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "messages")
public class Message extends BaseEntity {
    
    @ManyToOne
    private Account from;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private Account to;
    
    @Column(name = "date_sent")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateSent;
    
    @Column(name = "message", length = 512)
    private String message;

    
    public Message() {
        dateSent = new Date();
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
