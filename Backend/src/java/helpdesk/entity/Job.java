/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "job_list")
public class Job extends BaseEntity {
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "request")
    private ServiceRequest request;
    
    @Column(name = "closed")
    private boolean closed = false;
    
    @Column(name = "date_scheduled")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateScheduled;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assigned_contractor")
    private Contractor assignedContractor;
    
    @Column(name = "date_closed")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateClosed;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cancelled_by")
    private Account cancelledBy;
    
    @Column(name = "cancellation_reason")
    private String cancellationReason;
    
    
    public Job() {
        
    }
    
    public Job(ServiceRequest request, Contractor contractor) {
        this.request = request;
        this.assignedContractor = contractor;
    }
    
    public ServiceRequest getRequest() {
        return request;
    }

    public void setRequest(ServiceRequest request) {
        this.request = request;
    }
    
    public Date getDateScheduled() {
        return dateScheduled;
    }
    
    public void setDateScheduled(Date dateScheduled) {
        this.dateScheduled = dateScheduled;
    }

    public Contractor getAssignedContractor() {
        return assignedContractor;
    }

    public void setAssignedContractor(Contractor assignedContractor) {
        this.assignedContractor = assignedContractor;
    }

    public Date getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Account getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Account cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public boolean isClosed() {
        return closed;
    }

    public void closeJob() {
        this.closed = true;
        this.dateClosed = new Date();
    }
    
    public void cancelJob(Account cancelledBy, String message)
    {
        closeJob();
        this.cancellationReason = message;
        this.cancelledBy = cancelledBy;
    }
    
}
