    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "service_requests")
public class ServiceRequest extends BaseEntity {
    
    @Column(name = "available_dates")
    private String availability;
    
    @Column(name = "job_address")
    private String jobLocation;
    
    @Column(name = "requested_service")
    private String requestedService;
    
    @Column(name = "message")
    private String message;
    
    @Column(name = "min_price")
    private Double minPrice;
    
    @Column(name = "max_price")
    private Double maxPrice;
    
    @Column(name = "translator_required")
    private boolean tranlatorRequired;
    
    @Column(name = "date_requested")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateRequested;
    
    @Column(name = "deadline")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date deadline;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assigned_volunteer")
    private Volunteer assignedVolunteer;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "requester")
    private Migrant requester;
    
    @Column(name = "closed")
    private boolean closed = false;
    
    
    public ServiceRequest() {
        dateRequested = new Date();
    }
    
    public Volunteer getAssignedVolunteer() {
        return assignedVolunteer;
    }

    public void setAssignedVolunteer(Volunteer assignedVolunteer) {
        this.assignedVolunteer = assignedVolunteer;
    }
    
    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availableDates) {
        this.availability = availableDates;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getRequestedService() {
        return requestedService;
    }

    public void setRequestedService(String requestedService) {
        this.requestedService = requestedService;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isTranlatorRequired() {
        return tranlatorRequired;
    }

    public void setTranlatorRequired(boolean tranlatorRequired) {
        this.tranlatorRequired = tranlatorRequired;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = (minPrice == null) ? 0 : minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = (maxPrice == null) ? 999999.0 : maxPrice;
    }

    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public Migrant getRequester() {
        return requester;
    }

    public void setRequester(Migrant requester) {
        this.requester = requester;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }
    
}
