/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.entity;

import helpdesk.webservices.pojos.AccountModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Revan
 */
@Entity
@Table(name = "volunteers")
public class Volunteer extends Role {

    @Column(name = "assigned_requests")
    private Integer assignedRequests;
    
    public Volunteer() {
        
    }
    
    public Volunteer(AccountModel accountModel) {
        
    }
    
    public Integer getAssignedJobs() {
        return assignedRequests;
    }

    public void setAssignedJobs(Integer amount) {
        this.assignedRequests = amount;
    }

}
