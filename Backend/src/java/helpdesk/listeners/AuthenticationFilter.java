/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.listeners;

import helpdesk.services.AccountService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.ext.Provider;

import static helpdesk.Constants.URI_ROOT;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Revan
 */
@Provider
public class AuthenticationFilter implements javax.servlet.Filter {

    @EJB
    AccountService accountService;
    
    private Set<String> allowedUrls;
    
    private Set<String> getAllowedUrls() {
        if (allowedUrls == null) {
            allowedUrls = new HashSet<>();
            
            allowedUrls.add(URI_ROOT + "/login");
            allowedUrls.add(URI_ROOT + "/registration");
            allowedUrls.add(URI_ROOT + "/jackson");
        }
        
        return allowedUrls;
    }
    
    private void addHeaders(HttpServletResponse res) {
        res.setHeader("Access-Control-Allow-Origin", "http://ec2-54-153-228-105.ap-southeast-2.compute.amazonaws.com");
        res.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization");
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        res.setHeader("Access-Control-Max-Age", "1209600");
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        
        String uri = req.getRequestURI();
        
        System.out.println("Handling " + req.getMethod() + " request to " + uri);
        
        addHeaders(res);
        
        if (req.getMethod().equals("OPTIONS")) {
            res.setStatus(200);
            return;
        }
        
        // Service allowed to enter without being logged in
        if (getAllowedUrls().contains(uri)) {
            chain.doFilter(request, response);
            return;
        }
        
        HttpSession session = req.getSession(false);
        
        // User is not logged in
        if (session == null) {
            System.out.println("Authentication filter forbid request " + uri);
            res.setStatus(401);
            return;
        }
        
        chain.doFilter(request, response);
    }
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    
    @Override
    public void destroy() {
    }
}
