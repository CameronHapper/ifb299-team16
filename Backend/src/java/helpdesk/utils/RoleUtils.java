/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.utils;

import helpdesk.entity.Administrator;
import helpdesk.entity.Contractor;
import helpdesk.entity.Migrant;
import helpdesk.entity.Role;
import helpdesk.entity.Volunteer;

/**
 *
 * @author Revan
 */
public class RoleUtils {
    
    public static String roleToString(Role role) {
        if (role == null) return null;
        
        if (role instanceof Administrator) {
            return "administrator";
        }
        if (role instanceof Contractor) {
            return "contractor";
        }
        if (role instanceof Migrant) {
            return "migrant";
        }
        if (role instanceof Volunteer) {
            return "volunteer";
        }
        
        return null;
    }
    
    public static Class<? extends Role> stringToRole(String string) {
        if (string == null) return null;
        
        if ("administrator".equalsIgnoreCase(string)) {
            return Administrator.class;
        }
        if ("contractor".equalsIgnoreCase(string)) {
            return Contractor.class;
        }
        if ("migrant".equalsIgnoreCase(string)) {
            return Migrant.class;
        }
        if ("volunteer".equalsIgnoreCase(string)) {
            return Volunteer.class;
        }
        
        return null;
    }
    
}
