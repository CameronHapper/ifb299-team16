/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.webservices.pojos.ArrayTest;
import java.util.Arrays;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * This webservice is here to test, that tomEE is using the Jackson
 * jaxb provider, not the FUCKING RETARTED jettison. 
 */
@Path("/jackson")
public class JacksonTest {

    @GET
    public Response sayHelloJson() {
        return Response.status(Response.Status.OK).entity(Arrays.asList(new String[]{"Peter", "pan", "Ihihi"})).build();
    }
    
    @OPTIONS
    public Response options() {
        return Response.status(Response.Status.OK).build();
    }
    
    @POST
    @Path("array")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response arrayTest(ArrayTest test) {
        return Response.status(Response.Status.OK).entity(test).build();
    }
    
    @GET
    @Path("array")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response arrayTestGet() {
        ArrayTest test = new ArrayTest();
        test.setTest(Arrays.asList(new String[]{"Peter", "pan", "Ihihi"}));
        return Response.status(Response.Status.OK).entity(test).build();
    }
}
