/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import helpdesk.entity.Contractor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Revan
 */
public class ContractorList {
    
    private Integer count;
    
    private List<ContractorModel> contractors = new ArrayList<>();
    
    
    
    public ContractorList() {}
    
    public ContractorList(List<Contractor> contractors) {
        count = contractors.size();
        
        for(Contractor c : contractors) {
            this.contractors.add(new ContractorModel(c));
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<ContractorModel> getContractors() {
        return contractors;
    }

    public void setContractors(List<ContractorModel> contractors) {
        this.contractors = contractors;
    }
    
    
}
