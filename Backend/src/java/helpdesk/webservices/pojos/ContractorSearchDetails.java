/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

/**
 *
 * @author Revan
 */
public class ContractorSearchDetails {
    
    public String userName;
    
    public String firstName;
    
    public String lastName;
    
    public Integer min;
    
    public Integer max;
    
    public String service;
    
    public String address;
    
    public String email;

    public ContractorSearchDetails() {
    }

    public ContractorSearchDetails(String userName, String firstName, String lastName, Integer min, Integer max, String service, String address, String email) {
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.min = min;
        this.max = max;
        this.service = service;
        this.address = address;
        this.email = email;
    }
    
}
