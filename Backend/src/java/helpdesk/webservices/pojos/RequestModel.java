/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import helpdesk.entity.Migrant;
import helpdesk.entity.ServiceRequest;
import helpdesk.webservices.converters.DateConverter;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admins
 */
@XmlRootElement
public class RequestModel {
      
    private String availability;
    
    public String userName;
    
    public String jobLocation;
    
    public String requestedService;
    
    private String message;
    
    private PriceRange priceRange;
    
    public boolean translatorRequired;
    
    private String deadline;
    
    public String id;
    
    public RequestModel() {
        
    }
    
    public RequestModel(ServiceRequest request) {
        jobLocation = request.getJobLocation();
        requestedService = request.getRequestedService();
        userName = request.getRequester().getAccount().getUserName();
        id = request.getId();
        priceRange = new PriceRange(request.getMinPrice(),request.getMaxPrice());
        availability = request.getAvailability();
        deadline = DateConverter.dateToString(request.getDeadline());
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availableDates) {
        this.availability = availableDates;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getRequestedService() {
        return requestedService;
    }

    public void setRequestedService(String requestedService) {
        this.requestedService = requestedService;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PriceRange getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public boolean isTranslatorRequired() {
        return translatorRequired;
    }

    public void setTranslatorRequired(boolean tranlatorRequired) {
        this.translatorRequired = tranlatorRequired;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
    
    public static class PriceRange
    {
        protected Double min;
        
        protected Double max;
        
        public PriceRange() {
        }

        public PriceRange(Double min, Double max) {
            this.min = min;
            this.max = max;
        }
        

        public Double getMin() {
            return min;
        }

        public void setMin(Double min) {
            this.min = min;
        }

        public Double getMax() {
            return max;
        }

        public void setMax(Double max) {
            this.max = max;
        }
    }
    
}
