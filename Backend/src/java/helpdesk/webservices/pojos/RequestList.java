/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import helpdesk.entity.ServiceRequest;
import java.util.ArrayList;
import java.util.List;
import static org.apache.openejb.client.Client.request;

/**
 *
 * @author Revan
 */
public class RequestList {
    
    private Integer count;
    
    private List<RequestModel> requesters = new ArrayList<>();
    
    
    
    public RequestList() {}
    
    public RequestList(List<ServiceRequest> request) {
        count = request.size();
        
        for(ServiceRequest s : request) {
            this.requesters.add(new RequestModel(s));
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<RequestModel> getRequest() {
        return requesters;
    }

    public void setRequest(List<RequestModel> request) {
        this.requesters = request;
    }
    
    
}
