/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Revan
 */
@XmlRootElement
public class RequestAssignModel {
      
    private String requestId;
    
    private String contractor;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }
}
