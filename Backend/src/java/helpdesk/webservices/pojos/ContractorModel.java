/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import helpdesk.entity.Contractor;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Revan
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContractorModel {
    
    private List<String> services;

    private String chargeType;
    
    private Integer hourlyRate;
    
    private AccountModel account;
    
    
    
    public ContractorModel() {}
    
    public ContractorModel(Contractor contractor) {
        account = new AccountModel(contractor.getAccount());
        services = new ArrayList<>(contractor.getProffesions());
        chargeType = contractor.getChargeType();
        hourlyRate = contractor.getHourlyRate();
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Integer hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }
    
    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }
    
}
