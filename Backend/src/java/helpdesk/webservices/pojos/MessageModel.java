/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import helpdesk.entity.Message;
import helpdesk.webservices.converters.DateConverter;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Revan
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageModel {
    
    private String from;
    
    private String to;
    
    private String message;
    
    private String date;
    
    public MessageModel() { }
    
    public MessageModel(Message message) {
       this.from = message.getFrom().getUserName();
        this.to = message.getTo().getUserName();
        this.message = message.getMessage();
        this.date = DateConverter.timestampToString(message.getDateSent());
    }
    
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
