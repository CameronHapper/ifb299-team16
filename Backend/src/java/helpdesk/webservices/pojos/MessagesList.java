/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import helpdesk.entity.Message;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Revan
 */
public class MessagesList {
    
    private List<MessageModel> messages = new ArrayList<>();
    
    
    public MessagesList() { }
    
    public MessagesList(List<Message> messages) {
        for(Message message : messages) {
            this.messages.add(new MessageModel(message));
        }
    }

    public List<MessageModel> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageModel> messages) {
        this.messages = messages;
    }
    
}
