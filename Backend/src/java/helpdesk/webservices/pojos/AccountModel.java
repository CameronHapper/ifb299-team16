/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import helpdesk.entity.Account;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Revan
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountModel {
    
    private String userName;
    
    private String role;
    
    private String password;
    
    private String firstName;
    
    private String lastName;
    
    private String email;
    
    private String phone;
    
    private String address;
    
    private ContractorModel contractorDetails;
    
    public AccountModel() {}
    
    public AccountModel(Account account) {
        this.userName = account.getUserName();
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
        this.email = account.getEmail();
        this.phone = account.getPhone();
        this.address = account.getAddress();
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ContractorModel getContractorDetails() {
        return contractorDetails;
    }

    public void setContractorDetails(ContractorModel contractorDetails) {
        this.contractorDetails = contractorDetails;
    }
    
}
