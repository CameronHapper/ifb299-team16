/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import helpdesk.entity.Job;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admins
 */
public class JobList {
       
    private Integer count;
    
    private List<JobModel> jobs = new ArrayList<>();  
    
    public JobList() {}
    
    public JobList(List<Job> request) {
        count = request.size();
        
        for(Job s : request) {
            this.jobs.add(new JobModel(s));
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<JobModel> getJob() {
        return jobs;
    }

    public void setRequest(List<JobModel> job) {
        this.jobs = job;
    }
    
    
}

