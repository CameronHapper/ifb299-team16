/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import helpdesk.entity.Account;
import helpdesk.entity.Contractor;
import helpdesk.entity.Job;
import helpdesk.entity.ServiceRequest;
import helpdesk.webservices.converters.DateConverter;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admins
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobModel {
    
    private ServiceRequest request;
    
    private boolean closed = false;
    
    public String dateScheduled;
    
    public String assignedContractor;
    
    public String dateClosed;
    
    public String cancelledBy;
    
    private String cancellationReason;
    
    public String id;
    
    public JobModel() {}
    
    public JobModel(Job job) {
        //request = job.getRequest();
        dateScheduled = DateConverter.dateToString(job.getDateScheduled());
        assignedContractor = job.getAssignedContractor().getAccount().getUserName();
        dateClosed = DateConverter.dateToString(job.getDateClosed());
        cancelledBy = (job.getCancelledBy() == null) ? null : job.getCancelledBy().getUserName();
        id = job.getId();
    }
}
