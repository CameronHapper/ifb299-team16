/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.pojos;

/**
 *
 * @author Admins
 */
public class CurrentTaskSearchDetails {
    
    public String migrantName;
    
    public String jobLocation;
    
    public String requestedService;
    
    public boolean translatorRequired;
    
    public String contractorName;

    public CurrentTaskSearchDetails() {
    }

    public CurrentTaskSearchDetails(String migrantName, String jobLocation, 
            String requestedService, boolean translatorRequired, String contractorName) {
        this.migrantName = migrantName;
        this.jobLocation = jobLocation;
        this.requestedService = requestedService;
        this.translatorRequired = translatorRequired;
        this.contractorName = contractorName;
    }
    

    public String getMigrantName() {
        return migrantName;
    }

    public void setMigrantName(String migrantName) {
        this.migrantName = migrantName;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getRequestedService() {
        return requestedService;
    }

    public void setRequestedService(String requestedService) {
        this.requestedService = requestedService;
    }

    public boolean getTranslatorRequired() {
        return translatorRequired;
    }

    public void setTranslatorRequired(boolean translatorRequired) {
        this.translatorRequired = translatorRequired;
    }
    
    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }
}
