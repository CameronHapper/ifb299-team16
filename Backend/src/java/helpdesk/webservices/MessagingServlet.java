/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.  
 */
package helpdesk.webservices;

import helpdesk.entity.Message;
import helpdesk.services.MessagingService;
import helpdesk.webservices.pojos.MessageModel;
import helpdesk.webservices.pojos.MessagesList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
@Path("/message")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessagingServlet {

    
    @EJB
    MessagingService messagingService;
    
    @GET
    public Response getMessages(@QueryParam("from") String from,
                                @QueryParam("to") String to,
                                @DefaultValue("false") @QueryParam("bidirectional") boolean bidirectional) {
        
        List<Message> messages = messagingService.getMessages(from, to, bidirectional);
        
        MessagesList list = new MessagesList(messages);
        
        return Response.status(Response.Status.OK).entity(list).build();
    }
    
    @POST
    public Response sendMessage(MessageModel messageModel) {
        
        messagingService.sendMessage(messageModel);
        
        return Response.status(Response.Status.CREATED).build();
    }
    
}
