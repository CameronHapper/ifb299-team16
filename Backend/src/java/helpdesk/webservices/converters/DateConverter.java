/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Revan
 */
public class DateConverter {
    
    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy");
    }
    
    public static SimpleDateFormat getTimestampFormat() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    }
    
    public static String dateToString(Date date) {
        if (date == null) return null;
        return getDateFormat().format(date);
    }
    
    public static String timestampToString(Date date) {
        return getTimestampFormat().format(date);
    }
    
    public static Date stringToDate(String string) {
        try {
            return getDateFormat().parse(string);
        } catch (ParseException ex) {
            return null;
        }
    }
    
}
