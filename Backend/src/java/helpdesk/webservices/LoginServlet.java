/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.services.AccountService;
import helpdesk.services.AuthenticationService;
import helpdesk.utils.RoleUtils;
import helpdesk.webservices.pojos.LoginModel;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
@Path("/login")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class LoginServlet {

    @EJB
    AccountService accountService;
    
    @POST
    public Response login(LoginModel loginModel, @Context HttpServletRequest request) {
        
        accountService.login(loginModel);
        
        HttpSession session = request.getSession();
        session.setAttribute(AuthenticationService.SESSION_NAME, loginModel.getUserName());
        session.setAttribute(AuthenticationService.SESSION_ROLE, RoleUtils.stringToRole(loginModel.getRole()));
        
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @DELETE
    public Response logout(@Context HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
