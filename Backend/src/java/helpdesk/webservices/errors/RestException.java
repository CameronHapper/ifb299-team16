/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author Revan
 */
public class RestException extends WebApplicationException {
    
    private static final Status DEFAULT_CODE = Status.INTERNAL_SERVER_ERROR;
    
    public RestException(Status status) {
        super(status);
    }
    
    public RestException(Exception exception) {
        super(exception);
    }
    
    public RestException(String message) {
        super(Response.status(DEFAULT_CODE).entity(message).build());
    }
    
    public RestException(String message, Status status) {
        super(Response.status(status).entity(message).build());
    }
    
    public RestException(Exception exception, String message) {
        super(Response.status(DEFAULT_CODE).entity(message).build());
    }
}
