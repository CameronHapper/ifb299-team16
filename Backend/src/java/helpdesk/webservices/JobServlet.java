/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.entity.Contractor;
import helpdesk.entity.Job;
import helpdesk.entity.Migrant;
import helpdesk.entity.Role;
import helpdesk.services.JobService;
import helpdesk.webservices.pojos.JobList;
import helpdesk.webservices.pojos.RequestAssignModel;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admins
 */
@Stateless
@Path("/job")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class JobServlet {

    @EJB
    JobService jobService;
    
    @PUT
    public Response createJobFromRequest(RequestAssignModel model) {
        
        jobService.assignContractor(model.getRequestId(), model.getContractor());
        
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @DELETE
    @Path("{jobId}")
    public Response cancelJob(JobCancellationModel jobCancellation, @PathParam("jobId") String jobId) {
        
        String message = jobCancellation == null ? null : jobCancellation.getMessage();
        
        jobService.cancelJob(jobId, message);
        
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @GET
    @Path("/job_list/{role}/{userName}")
    public Response searchJobs (@PathParam("userName") String userName,
            @PathParam("role") String role){
        Class<? extends Role> roleClass;
        if(role.equals("contractor")) roleClass = Contractor.class;
        else roleClass = Migrant.class;
        
        List<Job> foundEntities = jobService.findTaskList(userName, roleClass);
 
        JobList job = new JobList(foundEntities);
        //ContractorList contractors = new ContractorList(foundEntities);
        
        return Response.status(Response.Status.OK).entity(job).build();
        //return null;
    }
          
}
