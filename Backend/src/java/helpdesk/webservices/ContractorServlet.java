/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.entity.Contractor;
import helpdesk.services.ContractorService;
import helpdesk.webservices.pojos.ContractorList;
import helpdesk.webservices.pojos.ContractorModel;
import helpdesk.webservices.pojos.ContractorSearchDetails;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
@Path("/contractor")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ContractorServlet {

    @EJB
    ContractorService contractorService;
    
    @GET
    @Path("/search")
    public Response searchContractors (
            @QueryParam("userName") String userName,
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName,
            @QueryParam("minPrice") Integer min,
            @QueryParam("maxPrice") Integer max,
            @QueryParam("service") String service,
            @QueryParam("address") String address,
            @QueryParam("email") String email) {
        
        List<Contractor> foundEntities = contractorService.search(new ContractorSearchDetails(userName, firstName, lastName, min, max, service, address, email));
        
        ContractorList contractors = new ContractorList(foundEntities);
        
        return Response.status(Response.Status.OK).entity(contractors).build();
    }
    
    @GET
    @Path("{userName}")
    public Response get(@PathParam("userName") String userName) {
        
        Contractor entity = contractorService.findByName(userName);
        
        if (entity == null) return Response.status(Response.Status.NOT_FOUND).build();
        
        ContractorModel contractor = new ContractorModel(entity);
        
        return Response.status(Response.Status.OK).entity(contractor).build();
    }
    
}
