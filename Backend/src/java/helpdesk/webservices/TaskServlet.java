/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.entity.ServiceRequest;
import helpdesk.services.AccountService;
import helpdesk.services.TaskService;
import helpdesk.webservices.pojos.RequestList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
@Path("/tasks")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TaskServlet {

    @EJB
    AccountService accountService;
    
    @EJB
    TaskService taskService;
    
    @GET
    @Path("/tasks/{userName}")
    public Response searchJobs (@PathParam("userName") String userName){
    
        List<ServiceRequest> foundEntities = taskService.findTaskList(userName);
 
        RequestList request = new RequestList(foundEntities);
        //ContractorList contractors = new ContractorList(foundEntities);
        
        return Response.status(Response.Status.OK).entity(request).build();
    }
      
    
    
}
