/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.webservices;

import helpdesk.services.AccountService;
import helpdesk.webservices.pojos.AccountModel;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
@Path("/registration")
public class RegistrationServlet {

    @EJB
    AccountService accountService;
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createAccount(AccountModel accountModel) {
        
        accountService.createAccount(accountModel);
        
        return Response.status(Response.Status.CREATED).build();
    }
}
