/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import helpdesk.entity.ServiceRequest;
import helpdesk.entity.Volunteer;
import helpdesk.webservices.errors.RestException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response;

/**
 *
 * @author admins
 */
@Stateless
public class TaskService {
    
    @PersistenceContext
    EntityManager em;
    
    @EJB
    AccountService accountService;

    @EJB
    RequestService requestService;
    
    @EJB
    AuthenticationService authenticationService;
    
    public List<ServiceRequest> findTaskList(String userName){//RequestModel details
        
        Volunteer vol = accountService.findRoleByName(userName, Volunteer.class);
        if (vol == null) throw new RestException(Response.Status.NOT_FOUND);
        
        TypedQuery<ServiceRequest> query = em.createQuery("SELECT request"
                + " FROM ServiceRequest request join request.assignedVolunteer vol "
                + "WHERE vol.id = :id", ServiceRequest.class);
        query.setParameter("id", vol.getId());
        List<ServiceRequest> results = query.getResultList();

        return results;
    }
}
