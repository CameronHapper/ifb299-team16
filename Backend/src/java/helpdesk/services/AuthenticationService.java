/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import helpdesk.entity.Account;
import helpdesk.entity.Role;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;

/**
 *
 * @author Revan
 */
@Stateless
public class AuthenticationService {
    
    public static String SESSION_NAME = "name";
    public static String SESSION_ROLE = "role";
    
    @Context
    private HttpServletRequest request;
    
    @EJB
    AccountService accountService;
    
    public String getLoggedUserName()
    {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        
        return (String) session.getAttribute(SESSION_NAME);
    }
    
    public Account getLoggedAccount()
    {
        String userName = getLoggedUserName();
        return accountService.findAccountByName(userName);
    }
    
    public <T extends Role> T getLoggedUserRole() {
        HttpSession session = request.getSession(false);
        if (session == null) return null;
        
        return (T) session.getAttribute(SESSION_ROLE);
    }
}
