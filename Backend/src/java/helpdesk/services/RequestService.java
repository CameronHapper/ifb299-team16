/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package helpdesk.services;

import helpdesk.entity.Migrant;
import helpdesk.entity.ServiceRequest;
import helpdesk.entity.Volunteer;
import helpdesk.webservices.converters.DateConverter;
import helpdesk.webservices.errors.RestException;
import helpdesk.webservices.pojos.RequestModel;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response;

/**
 *
 * @author admins
 */
@Stateless
public class RequestService {
   
    @PersistenceContext  
    EntityManager em;
    
    @EJB
    AccountService accountService;
    
    public ServiceRequest createRequest(RequestModel requestModel)
    {
      ServiceRequest request = getRequestBasedOnModel(requestModel);
      
      autoAssignVolunteer(request);
      
      em.persist(request);

      return request;
    }  
    
    public void autoAssignVolunteer(ServiceRequest request){
        
        TypedQuery<Volunteer> query = em.createQuery("SELECT a FROM Volunteer a ORDER BY a.assignedRequests ASC", Volunteer.class);
        List<Volunteer> results = query.getResultList();
        if((results.isEmpty()) == true){
            throw new RuntimeException("No volunteer found!");
        }
        request.setAssignedVolunteer(results.get(0));
    }
  
    private ServiceRequest getRequestBasedOnModel(RequestModel requestModel)
    {
        ServiceRequest request = new ServiceRequest();
        request.setAvailability(requestModel.getAvailability());
        request.setJobLocation(requestModel.getJobLocation());
        request.setRequestedService(requestModel.getRequestedService());
        RequestModel.PriceRange range = requestModel.getPriceRange();
        request.setMinPrice(range.getMin());
        request.setMaxPrice(range.getMax());
        request.setTranlatorRequired(requestModel.isTranslatorRequired());
        request.setDeadline(DateConverter.stringToDate(requestModel.getDeadline()));
        request.setMessage(requestModel.getMessage());
        
        Migrant migrant = accountService.findRoleByName(requestModel.getUserName(), Migrant.class);
        if (migrant == null) if (migrant == null) throw new RestException(Response.Status.NOT_FOUND);
        
        request.setRequester(migrant);
        
        return request;
    }
    
    public ServiceRequest getRequestById(String id) {
        return em.find(ServiceRequest.class, id);
    }
 
}
