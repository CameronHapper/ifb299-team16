/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import java.util.Random;

/**
 *
 * @author Revan
 */
public class IdGenerator {
    
    private static final int ID_LENGTH = 32;
    
    private static final char[] symbols;
    private static final Random random = new Random();
    
    
    static {
        StringBuilder tmp = new StringBuilder();
        
        for (char ch = '0'; ch <= '9'; ++ch) {
            tmp.append(ch);
        }
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            tmp.append(ch);
        }
        for (char ch = 'A'; ch <= 'Z'; ++ch) {
            tmp.append(ch);
        }
        
        symbols = tmp.toString().toCharArray();
    }
    
    

    public static String generateId() {
        StringBuilder builder = new StringBuilder(ID_LENGTH);
        
        for(int i = 0 ; i < ID_LENGTH ; i++) {
            builder.append(symbols[random.nextInt(symbols.length)]);
        }
        
        return builder.toString();
    }
}
