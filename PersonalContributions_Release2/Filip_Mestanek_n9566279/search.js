
function searchContractors() {
    var firstName = document.getElementById('firstNameInput').value;
    var lastName = document.getElementById('lastNameInput').value;
    var login = document.getElementById('loginInput').value;
    var address = document.getElementById('addressInput').value;
    var service = document.getElementById('serviceInput').value;
    var rate = document.getElementById('rateInput').value;
    var email = document.getElementById('emailInput').value;
    
    var queryParams = [];
    
    addParam(queryParams, firstName, "firstName");
    addParam(queryParams, lastName, "lastName");
    addParam(queryParams, login, "userName");
    addParam(queryParams, address, "address");
    addParam(queryParams, service, "service");
    addParam(queryParams, rate, "");
    addParam(queryParams, email, "email");
    
    var url = buildUrl(queryParams);
    
    queryContractors(url);
}

function addParam(params, value, name) {
    if (value === "") return;
    
    var obj = { name: name, value: value};
    params.push(obj);
}

function buildUrl(queryParams) {
    var url = "contractor/search";
    
    if (queryParams.length === 0) return url;
    
    url += "?";
    
    for(var i = 0 ; i < queryParams.length ; i++) {
        url += queryParams[i].name + "=" + queryParams[i].value;
        if (i < (queryParams.length - 1)) url += "&";
    } 
    
    return url;
}

function queryContractors(url) {
    var callback = function (xhr) {
        if (xhr.status === 200) {
            displayContractors(xhr.responseText);
        }
        else httpError(xhr);
    };
    
    sendData("GET", url, null, callback);
}

function displayContractors(response) {
    var contractors = JSON.parse(response);
    var contractors = contractors.contractors;
    
    var table = document.getElementById("searchResultsTable");
    clearTable(table, true);
    
    for(var i = 0 ; i < contractors.length ; i++) {
        var contractor = contractors[i];
        var account = contractor.account;
        
        var row = table.insertRow(i + 1);
        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
        var cell3 = row.insertCell(3);
        var cell4 = row.insertCell(4);
        
        setTextOfLabel(cell0, account.userName);
        setTextOfLabel(cell1, account.firstName);
        setTextOfLabel(cell2, account.lastName);
        setTextOfLabel(cell3, contractor.services);
        
        
        setTextOfLabel(cell4, '<a href="contractor.html" onclick="localStorage.selectedContractor=\'' + account.userName + '\'">details</a>');
    }
    
}