/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import helpdesk.entity.Account;
import helpdesk.entity.Message;
import helpdesk.webservices.errors.RestException;
import helpdesk.webservices.pojos.MessageModel;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
public class MessagingService {
    
    @PersistenceContext
    EntityManager em;
    
    @EJB
    AccountService accountService;
    
    public void sendMessage(MessageModel messageModel) {
        Message message = createMessageFromModel(messageModel);
        em.persist(message);
    }
    
    private Message createMessageFromModel(MessageModel messageModel) {
        Message message = new Message();
        
        message.setMessage(messageModel.getMessage());
        Account from = accountService.findAccountByName(messageModel.getFrom());
        Account to = accountService.findAccountByName(messageModel.getTo());
        
        if (from == null || to == null) throw new RestException(Response.Status.NOT_FOUND);
        if (from.equals(to)) throw new RestException(Response.Status.BAD_REQUEST);
        
        message.setFrom(from);
        message.setTo(to);
        
        return message;
    }
    
    public List<Message> getMessages(String from, String to, boolean bidirectional) {
        
        Account accountFrom = accountService.findAccountByName(from);
        Account accountTo = accountService.findAccountByName(to);
        
        if (accountFrom == null || accountTo == null) throw new RestException(Response.Status.NOT_FOUND);
        
        List<Message> messages = retrieveMessages(accountFrom, accountTo);
        if (bidirectional) messages.addAll(retrieveMessages(accountTo, accountFrom));
        
        return messages;
    }
    
    private List<Message> retrieveMessages(Account from, Account to) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        
        CriteriaQuery<Message> query = cb.createQuery(Message.class);
        Root<Message> root = query.from(Message.class);
        
        query.select(root).where(
            cb.equal(root.get("from"), from),
            cb.equal(root.get("to")  , to  )
        );
        
        TypedQuery<Message> typedQuery = em.createQuery(query);
        List<Message> results = typedQuery.getResultList();
        
        return results;
    }
} 
