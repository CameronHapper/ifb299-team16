/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;


import helpdesk.entity.Account;
import helpdesk.entity.Contractor;
import helpdesk.entity.Job;
import helpdesk.entity.Role;
import helpdesk.webservices.errors.RestException;
import helpdesk.entity.ServiceRequest;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;


/**
 *
 * @author Admins
 */
@Stateless
public class JobService {
    
    @PersistenceContext
    EntityManager em;
    
    @EJB
    AccountService accountService;

    @EJB
    RequestService requestService;
    
    @EJB
    AuthenticationService authenticationService;
    
    public List<Job> findTaskList(String userName, Class<? extends Role> role){//RequestModel details
        
        Account vol = accountService.findAccountByName(userName);
        if (vol == null) throw new RestException(Response.Status.NOT_FOUND);
        
        TypedQuery<Job> query;
        
        if(role.equals(Contractor.class)){
            query = em.createQuery("SELECT request"
                + " FROM Job request join request.assignedContractor.account cont "
                + "WHERE cont.id = :id", Job.class);
            query.setParameter("id", vol.getId());
        }else{
            query = em.createQuery("SELECT request"
                + " FROM Job request join request.request.requester.account mig "
                + "WHERE mig.id = :id", Job.class);
            query.setParameter("id", vol.getId());
        }
        List<Job> results = query.getResultList();

        return results;
    }
    
    public void assignContractor(String requestId, String contractorName) {
        
        Contractor contractor = accountService.findRoleByName(contractorName, Contractor.class);
        if (contractor == null) throw new RestException(Response.Status.NOT_FOUND);
        
        ServiceRequest request = requestService.getRequestById(requestId);
        if (request == null) throw new RestException(Response.Status.NOT_FOUND);
        
        if (!canCreateJobFromRequest(request)) {
            throw new RestException(Response.Status.BAD_REQUEST);
        }
        
        createJobFromRequest(request, contractor);
    }
    
    /**
     * Returns, whether it is possible to create a job from this request
     */
    private boolean canCreateJobFromRequest(ServiceRequest request) {
        
        if (request.isClosed()) {
            return false;
        }
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        
        CriteriaQuery<Job> query = cb.createQuery(Job.class);
        Root<Job> root = query.from(Job.class);
        
        query.select(root).where(
            cb.and(
                cb.equal(root.get("request").get("id"), request.getId()),
                cb.equal(root.get("closed"), false)
            )
        );
        
        TypedQuery<Job> typedQuery = em.createQuery(query);
        List<Job> results = typedQuery.getResultList();
        
        return results.isEmpty();
    }
    
    public void createJobFromRequest(ServiceRequest request, Contractor contractor)
    {
        Job job = new Job(request, contractor);
        em.persist(job);
    }

   /* public Migrant findByName(String userName) {
        Migrant migrant = accountService.findRoleByName(userName, Migrant.class);
        
        if (migrant == null) return null;
        
        return migrant;
    }*/
    
    public void cancelJob(String jobId, String message)
    {
        Job job = em.find(Job.class, jobId);
        
        if (job == null) throw new RestException(Response.Status.NOT_FOUND);
        if (job.isClosed()) throw new RestException(Response.Status.BAD_REQUEST);
        if (job.getRequest().isClosed()) throw new RestException(Response.Status.BAD_REQUEST);
        
        Account account = authenticationService.getLoggedAccount();
        
        job.cancelJob(account, message);
    }
    
}
