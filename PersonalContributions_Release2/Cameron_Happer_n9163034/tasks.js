function displayJobs(data) {//data
    var jobs = JSON.parse(data);
	var jobs = jobs.request;
		
    var table = document.getElementById("taskList"); 
	var table2 = document.getElementById("taskList2");
	
    for(var i = 0 ; i < jobs.length ; i++) {//jobs.length
          
        var row = table.insertRow(i + 1);//i + 1
        var cell10 = row.insertCell(0);
        var cell11 = row.insertCell(1);
        var cell12 = row.insertCell(2);
		var cell13 = row.insertCell(3);
		var cell14 = row.insertCell(4);
		var cell15 = row.insertCell(5);
		var cell16 = row.insertCell(6);
		
		var row2 = table2.insertRow(i + 1);
		var cell20 = row2.insertCell(0);
        var cell21 = row2.insertCell(1);
		//var cell6 = row.insertCell(6);

        cell10.innerHTML = i;
        cell11.innerHTML = jobs[i].userName;//"contractorX";
        cell12.innerHTML = jobs[i].jobLocation;//"90 satan street";
        cell13.innerHTML = jobs[i].requestedService;//"carpenter";
		cell14.innerHTML = jobs[i].priceRange.min + " - " + jobs[i].priceRange.max;
		cell15.innerHTML = jobs[i].deadline;
		cell16.innerHTML = jobs[i].availability;
		
		cell20.innerHTML = i;//"contractorX";
		cell21.innerHTML = jobs[i].id;
    }
	//sendData("GET", 'job_list/job_list', null, callback);
} //addWindowOnloadFunction(displayJobs);

function loadTasks() {
    
    var callback = function (xhr) {
        if (xhr.status === 200) {
            displayJobs(xhr.responseText);
        }
        else httpError(xhr);
    };
       
    sendData("GET", 'tasks/tasks/' + localStorage.userName, null, callback);
    
} addWindowOnloadFunction(loadTasks);

