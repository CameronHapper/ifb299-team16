
function messageTo() {
    var messageTo = document.getElementById('messageToInput').value;
    
    loadMessageHistory(localStorage.userName, messageTo);
}

function loadMessageHistory(from, to) {
    var callback = function (xhr) {
        if (xhr.status === 200) {
            localStorage.messageTarget = to;
            displayMessageHistory(xhr.responseText);
        } else if (xhr.status === 404) {
            showMessage("User you are trying to contact doesn't exist!");
        }
        else httpError(xhr);
    };
    
    var url = "message?from=" + from + "&to=" + to + "&bidirectional=true";
    
    sendData("GET", url, null, callback);
}

function displayMessageHistory(data) {
    setVisible(document.getElementById('writeMessageForm'), true);
    
    var messageHistory = JSON.parse(data);
    var messages = messageHistory.messages;
    
    var messageHistoryEl = document.getElementById('messageHistory');
    messageHistoryEl.innerHTML = "";
    
    messages.sort(function(a, b) {
        return compareDates(stringToDate(b.date), stringToDate(a.date));
    });
    
    for(var i = 0 ; i < messages.length ; i++) {
        var message = messages[i];
        
        messageHistoryEl.innerHTML += buildMessage(message);
    }
    
    $("#messageHistory").height(Math.max($("#menu").height(), 500));
}

function buildMessage(message) {
    var messageClass = (message.from === localStorage.userName) ? "mine" : "theirs";
    
    return "<fieldset class='" + messageClass + "'><legend>" + message.date + "</legend>" + message.message + "</fieldset>";
}

function writeMessage() {
    var message = document.getElementById('messageInput').value;
    
    var jsonObject = new Object();
    jsonObject.from = localStorage.userName;
    jsonObject.to = localStorage.messageTarget;
    jsonObject.message = message;
    
    var callback = function(xhr) {
        if (xhr.status === 201) {
            showMessage("Message successfully sent");
            
            loadMessageHistory(jsonObject.from, jsonObject.to);
        }
        else httpError(xhr);
    };
    
    sendData("POST", "message", jsonObject, callback);
}
