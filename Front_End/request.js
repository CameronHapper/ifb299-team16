
function validatePriceRange() {
     var min = document.getElementById("priceMinInput").value;
     var max = document.getElementById("priceMaxInput").value;
     
     if (max < min) {
         showMessage("Min price needs to be lower than max price");
         return false;
     }
     
     return true;
}

function submitRequest() {
    if (!validatePriceRange()) return false;
    
    var requestForm = document.getElementById("requestForm");
    
    // Get service
    var serviceVal = getRadioVal(requestForm, "service");
    if (serviceVal === "other") {
        serviceVal = document.getElementById("otherServiceInput").value;
    }
    
    // Get min and max price
    var min = document.getElementById("priceMinInput").value;
    var max = document.getElementById("priceMaxInput").value;
    
    // Init the object
    var jsonObject = new Object();
    jsonObject.userName = localStorage.userName;
    jsonObject.requestedService = serviceVal;
    jsonObject.jobLocation = document.getElementById("jobLocationInput").value;
    jsonObject.availability = document.getElementById("availabilityInput").value;
    jsonObject.priceRange = {min: min === "" ? null : min, max: max === "" ? null : max};
    jsonObject.deadline = document.getElementById("deadlineInput").value;
    jsonObject.translatorRequired = document.getElementById("translatorCheckbox").checked;
    jsonObject.message = document.getElementById("messageTextarea").value;
    
    var callback = function(xhr) {
        if (xhr.status === 201) {
            showMessage("Request was successfully created!");
            refreshPage();
        }
        else httpError(xhr);
    };
    
    sendData("POST", 'service', jsonObject, callback);
}
