
function loadContractor() {
    
    var callback = function (xhr) {
        if (xhr.status === 200) {
            displayContractor(xhr.responseText);
        }
        else httpError(xhr);
    };
    
    if (localStorage.selectedContractor === undefined) {
        alert("No contractor defined!");
    }
    
    sendData("GET", 'contractor/' + localStorage.selectedContractor, null, callback);
    
} addWindowOnloadFunction(loadContractor);

function displayContractor(data) {
    var contractor = JSON.parse(data);
    var account = contractor.account;
    
    setAccountData(account);
    setContractorData(contractor);
}

function setContractorData(contractor) {
    setTextOfLabel(document.getElementById("servicesInput"), contractor.services);
    
    var chargeRate;
    
    if (contractor.chargeType === "hour") {
        chargeRate = contractor.hourlyRate + " / hour";
    } else {
        chargeRate = "by contract";
    }
    
    setTextOfLabel(document.getElementById("rateInput"), chargeRate);
}

function setAccountData(account) {
    setTextOfLabel(document.getElementById("loginInput"), account.userName);
    setTextOfLabel(document.getElementById("firstNameInput"), account.firstName);
    setTextOfLabel(document.getElementById("lastNameInput"), account.lastName);
    setTextOfLabel(document.getElementById("addressInput"), account.email);
    setTextOfLabel(document.getElementById("phoneInput"), account.phone);
    setTextOfLabel(document.getElementById("emailInput"), account.address);
}