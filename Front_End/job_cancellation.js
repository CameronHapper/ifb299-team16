
function cancelJob() {
    var jobId = document.getElementById("jobInput").value;
    var message = document.getElementById("messageInput").value;
    
    var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Job " + jobId + " cancelled");
        }
        else httpError(xhr);
    };
    
    var jsonObject = new Object();
    jsonObject.message = message;
    
    sendData("DELETE", 'job/' + jobId, jsonObject, callback);
}
