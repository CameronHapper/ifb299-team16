
function trimArray(stringArray) {
    for(var i = 0 ; i < stringArray.length ; i++) {
        stringArray[i] = stringArray[i].trim();
    }
    
    return stringArray;
}

// Submit registration form
function registerSendData() {

    if (!validatePasswords()) return false;
    
    var registerForm = document.getElementById("registerForm");

    var jsonObject = new Object();
    jsonObject.userName = document.getElementById("loginInput").value;
    jsonObject.role = document.getElementById("roleSelect").value;
    jsonObject.firstName = document.getElementById("firstNameInput").value;
    jsonObject.lastName = document.getElementById("lastNameInput").value;
    jsonObject.email = document.getElementById("emailInput").value;
    jsonObject.phone = document.getElementById("phoneInput").value;
    jsonObject.address = document.getElementById("addressInput").value;
    jsonObject.password = document.getElementById("passwordInput").value;
    
    if (jsonObject.role === "contractor") {
        var contractorDetails = new Object();
        jsonObject.contractorDetails = contractorDetails;
        
        var services = document.getElementById("contractorServicesInput").value;
        services = services.split(",");
        services = trimArray(services);
        
        contractorDetails.services = services;
        contractorDetails.chargeType = getRadioVal(registerForm, "paytype");
        
        var hourlyRate = null;
        if (contractorDetails.chargeType === "hour") hourlyRate = document.getElementById("hourlyRateInput").value;
        contractorDetails.hourlyRate = hourlyRate;
    }

    var callback = function (xhr) {
        if (xhr.status === 201) {
            showMessage("User " + jsonObject.userName + " successfully created");
            refreshPage();
        }
        else if (xhr.status === 409) {
            showMessage("User " + jsonObject.userName + " already exists!");
        }
        else
            httpError(xhr);
    };

    sendData("POST", 'registration', jsonObject, callback);
}

function validatePasswords() {

    var password = document.getElementById("passwordInput");
    var password2 = document.getElementById("passwordConfirmInput");

    if (password.value !== password2.value) {
        password2.focus();
        showMessage("Passwords differ!");
        return false;
    }

    return true;
}

function registerRolesSelect() {
    var role = document.getElementById("roleSelect").value;
    var contractorDiv = document.getElementById("contractorDetails");

    var contractorSelected = (role === "contractor");

    setVisible(contractorDiv, contractorSelected);
    
    document.getElementById("contractorServicesInput").required = contractorSelected;
}

function chargeSystemChange() {
    var hourlyRateRadio = document.getElementById("hourlyRateRadio");
    var hourlyRateDiv = document.getElementById("hourlyRate");

    setVisible(hourlyRateDiv, hourlyRateRadio.checked);
}
