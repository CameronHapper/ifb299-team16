
var onloadFunctions = [];

function addWindowOnloadFunction(func) {
    onloadFunctions.push(func);
}

window.onload = function() {
    for(var i = 0 ; i < onloadFunctions.length ; i++) {
        onloadFunctions[i]();
    }
};

function logoutLoginControls() {
    if (localStorage.loggedIn === undefined) {
        performLogOut();
    }
    
    loginLogoutVisibility();
	
} addWindowOnloadFunction(logoutLoginControls);

function loginLogoutVisibility() {
    var logInPortlet = document.getElementById("loginPortlet");
    var logOutPortlet = document.getElementById("logoutPortlet");
    
    setVisible(logInPortlet,  (localStorage.loggedIn !== "true"));
    setVisible(logOutPortlet, (localStorage.loggedIn === "true"));
    
    setTextOfLabel(document.getElementById('loggedAs'), localStorage.loggedAs);
	
    changeMenu();
}

function clearLoginForm() {
    document.getElementById('loginPortlet').reset();
}

// Wrapper to sending data to server
function sendData(httpMethod, url, data, callback) {
    
    var baseUrl = "http://ec2-54-153-228-105.ap-southeast-2.compute.amazonaws.com:8080/helpdesk/";
    var fullUrl = baseUrl + url;

    console.log("sending data. Method: " + httpMethod + " url: " + fullUrl);

    var xhr = new XMLHttpRequest();
    xhr.open(httpMethod, fullUrl);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.withCredentials = true;

    xhr.onreadystatechange = function () {
        if (this.readyState === 4) {
            callback(this);
        }
    };

    if (data !== null) xhr.send(JSON.stringify(data));
    else xhr.send();
}

function showMessage(message) {
    alert(message);
}

function refreshPage() {
    location.reload();
}

function redirectTo(url) {
    location.href = "index.html";
}

function httpError(xhr) {
    if (xhr.status === 401) {
        showMessage("Unauthorized! Please log in.");
        performLogOut();
        loginLogoutVisibility();
    }
    else if (xhr.status === 403) {
        showMessage("Forbidden! Insufficient rights to perform the action.");
    }
    else if (xhr.status === 404) {
        showMessage("Not found!");
    }
    else {
        showMessage("Unexpected error occurred. Status: " + xhr.status + " Response: " + xhr.responseText);
    }
}

function performLogOut() {
    localStorage.loggedIn = "false";
    localStorage.loggedAs = null;
    localStorage.userName = null;
}

function logIn() {
    var loginName = document.getElementById("loginName").value;
    var loginPass = document.getElementById("loginPass").value;
    var loginRole = document.getElementById("loginRoleSelect").value;

    var jsonObject = new Object();
    jsonObject.userName = loginName;
    jsonObject.password = loginPass;
    jsonObject.role = loginRole;
    
    var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Successfully logged in");
            localStorage.loggedIn = "true";
            localStorage.loggedAs = jsonObject.role;
            localStorage.userName = jsonObject.userName;
            loginLogoutVisibility();
        }
        else if (xhr.status === 403 || xhr.status === 404) {
            showMessage("Login failed!");
        }
        else httpError(xhr);
        
        clearLoginForm();
    };
    
    sendData("POST", 'login', jsonObject, callback);	
	
}

function logOut() {
    
	var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Successfully logged out");
            performLogOut();
            redirectTo("www.seznam.cz");
            //loginLogoutVisibility();
        }
        else httpError(xhr);
    };
    
    sendData("DELETE", 'login', null, callback);
}

// Returns value of radio button of the form in group "group"
function getRadioVal(form, name) {
    var val;
    // get list of radio buttons with specified name
    var radios = form.elements[name];
    
    // loop through list of radio buttons
    for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) { // radio checked?
            val = radios[i].value; // if so, hold its value in val
            break; // and break out of for loop
        }
    }
    return val; // return value of checked radio or undefined if none checked
}

function setVisible(element, visible) {
    if (visible) element.style.display = "block";
    else element.style.display = "none";
}

function setTextOfLabel(element, text) {
    element.innerHTML = text;
}

function clearTable(table, preserveFirstRow) {
    var i = (preserveFirstRow) ? 1 : 0;
    
    for(var i = 1; i < table.rows.length;)
    {
       table.deleteRow(i);
    }
}

function stringToDate(dateString) {
    var regex = /(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/;
    var dateArray = regex.exec(dateString); 
    var dateObject = new Date(
    (+dateArray[3]),
    (+dateArray[2])-1, // Month starts at 0!
    (+dateArray[1]),
    (+dateArray[4]),
    (+dateArray[5]),
    (+dateArray[6])
    );
    
    return dateObject;
}

function compareDates(a, b) {
    if (a.getTime() < b.getTime()) return -1;
    else return 1;
}

// Menu Change

function changeMenu(){
	
        var guestMenu = "guest_menu";
        var migrantMenu = "migrant_menu";
        var contractorMenu = "contractor_menu";
        var adminMenu = "admin_menu";
        var volunteerMenu = "volunteer_menu";
        
        setVisible(document.getElementById(guestMenu), false);
        setVisible(document.getElementById(migrantMenu), false);
        setVisible(document.getElementById(contractorMenu), false);
        setVisible(document.getElementById(adminMenu), false);
        setVisible(document.getElementById(volunteerMenu), false);
        
        
	if (localStorage.loggedAs === "migrant") {
		setVisible(document.getElementById(migrantMenu), true);
	} else if (localStorage.loggedAs === "contractor") {
		setVisible(document.getElementById(contractorMenu), true);
	} else  if (localStorage.loggedAs === "volunteer") {
		setVisible(document.getElementById(volunteerMenu), true);
	} else {
		setVisible(document.getElementById(guestMenu), true);
	}

} addWindowOnloadFunction(changeMenu);