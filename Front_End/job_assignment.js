
function assignJob() {
    var requestId = document.getElementById("requestInput").value;
    var contractorName = document.getElementById("contractorInput").value;
    
    var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Contractor " + contractorName + " assigned");
        } else if (xhr.status === 400) {
            showMessage("Cannot assign the task!");
        }
        else httpError(xhr);
    };
    
    var jsonObject = new Object();
    jsonObject.requestId = requestId;
    jsonObject.contractor = contractorName;
    
    sendData("PUT", 'job', jsonObject, callback);
}
