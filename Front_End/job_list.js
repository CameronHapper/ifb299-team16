function displayJobs(data) {//data
    var jobs = JSON.parse(data);
	var jobs = jobs.job;
		
    var table = document.getElementById("jobList"); 
    var table2 = document.getElementById("jobList2"); 
	
    for(var i = 0 ; i < jobs.length ; i++) {//jobs.length
          
        var row = table.insertRow(i + 1);//i + 1
        var cell0 = row.insertCell(0);
        var cell1 = row.insertCell(1);
        var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(3);
        
		cell0.innerHTML = i;
        cell1.innerHTML = jobs[i].assignedContractor;//"contractorX";
        cell2.innerHTML = jobs[i].dateScheduled;//"90 satan street";
        cell3.innerHTML = jobs[i].dateClosed;//"carpenter";
        cell4.innerHTML = jobs[i].cancelledBy;
		
		var row2 = table2.insertRow(i + 1);
		var cell10 = row2.insertCell(0);
        var cell11 = row2.insertCell(1);
		
		cell10.innerHTML = i;
		cell11.innerHTML = jobs[i].id;
    }
	//sendData("GET", 'job/job_list/', null, callback);
} //addWindowOnloadFunction(displayJobs);

function loadTasks() {
    
    var callback = function (xhr) {
        if (xhr.status === 200) {
            displayJobs(xhr.responseText);
        }
        else httpError(xhr);
    };
       
    sendData("GET", 'job/job_list/' + localStorage.role + "/" + localStorage.userName, null, callback);
    
} addWindowOnloadFunction(loadTasks);

