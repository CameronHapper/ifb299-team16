


// Validate Register Form

var validate = true;

function registerValidate() {
	
	
//	checkUserType();
	checkFName();
	checkLName();
	checkAddress();
	checkPhone();
	checkEmail();
//	checkUsername();
	checkPassword();
	checkConPassword();
//	return false;
	
	
	
	if (validate == true) {
		//window.alert("Submission Successful!");	
		//	postData();
		var registerLocation = "register_success.html";
		window.location = registerLocation;
	} else {
		return false;
	}
	
}

function checkUserType() {
	if (register.contractor.checked == false &&  register.migrant.checked == false && register.volunteer.checked == false){				
		document.getElementById("utypeMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkFName() {
	if (register.fname.value== "") {
		document.getElementById("fnameMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkLName() {
	if (register.lname.value== "") {
		document.getElementById("lnameMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkAddress() {
	if (register.address.value== "") {
		document.getElementById("addressMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkPhone() {
	if (register.phone.value== "") {
		document.getElementById("phoneMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkEmail() {
	if (register.email.value== "") {
		document.getElementById("emailMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkUsername() {
	if (register.uname.value== "") {
		document.getElementById("unameMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkPassword() {
	if (register.pword.value== "") {
		document.getElementById("pwordMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkConPassword() {
	if (register.pwordc.value== "") {
		document.getElementById("pwordcMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}


// Validate Request Form

//var validate = true;

function requestValidate() {
	
	
	//checkServiceType();
	checkLocation() ;
	checkTime();
	checkPrice();
	checkDeadline();

//	return false;
	
	if (validate == true) {
		//window.alert("Submission Successful!");		
		var requestLocation = "request_success.html";
		window.location = requestLocation;
	} else {
		return false;
	}
	
}

function checkServiceType() {
	if (request.carpenter.checked == false &&  register.electrician.checked == false && register.roofer.checked == false && register.bricklayer.checked == false && register.plumber.checked == false && register.stype.value== ""){				
		document.getElementById("stypeMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkLocation() {
	if (request.location.value== "") {
		document.getElementById("locMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkTime() {
	if (request.time.value== "" || request.time.value=="eg: Mon 11am-3pm") {
		document.getElementById("timeMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

function checkPrice() {
	if (request.price.value== "" || request.price.value=="eg: $400-700") {
		document.getElementById("priceMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}	

function checkDeadline() {
	if (request.deadline.value== "" || request.price.value=="eg:17/10/15") {
		document.getElementById("deadMissing").style.visibility = "visible";		
		validate = false;
	}
	else
	{
		validate = true;
	}
}

// Webpage to Server

//function postData(JSONData, localMode)     {
 //       var localJSONData = JSONData;
 //       var postMode = localMode;
//
 //        $.ajax({
 //               type:'POST',
 //               url:'http://localhost:8080/helpdesk/registration',
 //               contentType:"application/json; charset=utf-8",
 //               dataType:"json",
 //               data:JSONData,
 //               success:function(data){
 //                   alert("SECOND POST JSON DATA");
 //               }
//		});

}


	