/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.  
 */
package helpdesk.webservices;

import helpdesk.services.RequestService;
import helpdesk.webservices.pojos.RequestModel;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admins
 */
@Stateless
@Path("/service")
public class RequestServlet {

    
    @EJB
    RequestService requestService;
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createService(RequestModel requestModel) {
        
        requestService.createRequest(requestModel);
        
        return Response.status(Response.Status.NO_CONTENT).build();
    }
    
    @GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public RequestModel test() {
        RequestModel m = new RequestModel();
        
        m.setRequestedService("Carpenter");
        m.setJobLocation("123 Peanut drive");
        m.setAvailability("tuesday");
        m.setUserName("login1");
        m.setPriceRange(new RequestModel.PriceRange(200.0, 500.0));
        m.setDeadline("17/05/2015");
        m.setTranslatorRequired(true);
        m.setRequestedService("carpenter");
        m.setJobLocation("10 tradie way brisbane");
        
        return m;
    }

}
