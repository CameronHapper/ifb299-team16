/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package helpdesk.services;

import helpdesk.entity.ServiceRequest;
import helpdesk.entity.Volunteer;
import helpdesk.webservices.pojos.RequestModel;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author admins
 */
@Stateless
public class RequestService {
   
    @PersistenceContext  
    EntityManager em;
    
    public ServiceRequest createRequest(RequestModel requestModel)
    {
      ServiceRequest request = getRequestBasedOnModel(requestModel);
      
      autoAssignVolunteer(request);
      
      em.persist(request);

      return request;
    }  
    
    public void autoAssignVolunteer(ServiceRequest request){
        
        TypedQuery<Volunteer> query = em.createQuery("SELECT a FROM Volunteer a ORDER BY a.assignedRequests ASC", Volunteer.class);
        query.setParameter("null", request);
        List<Volunteer> results = query.getResultList();
        if((results.isEmpty()) == true){
            
        }
        request.setAssignedVolunteer(results.get(0));
    }
    
    private ServiceRequest getRequestBasedOnModel(RequestModel requestModel)
    {
        ServiceRequest request = new ServiceRequest();
        request.setAvailability(requestModel.getAvailability());
        request.setJobLocation(requestModel.getJobLocation());
        request.setRequestedService(requestModel.getRequestedService());
        RequestModel.PriceRange range = requestModel.getPriceRange();
        request.setMinPrice(range.getMin());
        request.setMaxPrice(range.getMax());
        request.setTranlatorRequired(requestModel.isTranslatorRequired());
        request.setDeadline(requestModel.getDeadline());
        
        return request;
    }

 
}
