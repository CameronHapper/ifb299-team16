/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import helpdesk.entity.Contractor;
import helpdesk.webservices.pojos.ContractorList;
import helpdesk.webservices.pojos.ContractorModel;
import helpdesk.webservices.pojos.ContractorSearchDetails;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Revan
 */
@Stateless
public class ContractorService {
    
    @PersistenceContext
    EntityManager em;
    
    @EJB
    AccountService accountService;
    
    public ContractorList search(ContractorSearchDetails details)
    {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        
        CriteriaQuery<Contractor> query = cb.createQuery(Contractor.class);
        Root<Contractor> root = query.from(Contractor.class);
        
        // Build the query
        List<Predicate> predicates = new ArrayList<>();
        
        if (details.getUserName() != null) {
            predicates.add(cb.equal(root.get("account").get("userName"), details.getUserName()));
        }
        if (details.getFirstName() != null) {
            predicates.add(cb.equal(root.get("account").get("firstName"), details.getFirstName()));
        }
        if (details.getLastName() != null) {
            predicates.add(cb.equal(root.get("account").get("lastName"), details.getLastName()));
        }
        if (details.getService() != null) {
            List<String> services = Arrays.asList(new String[] {details.getService()});
            predicates.add(root.join("proffesions").in(services));
        }
        
        
        query.select(root).where(predicates.toArray(new Predicate[] {}));
        
        
        // Run the query
        TypedQuery<Contractor> typedQuery = em.createQuery(query);
        List<Contractor> results = typedQuery.getResultList();
        
        return new ContractorList(results);
   }
    
    public ContractorModel findByName(String userName) {
        Contractor contractor = accountService.findRoleByName(userName, Contractor.class);
        
        if (contractor == null) return null;
        
        return new ContractorModel(contractor);
    }
}
