
window.onload = function() {
    var logInPortlet = document.getElementById("login");
    var logOutPortlet = document.getElementById("logout");
    
    if (localStorage.loggedIn === undefined) {
        localStorage.loggedIn = "false";
    }
    
    if (localStorage.loggedIn === "true") {
        logInPortlet.style.display = "none";
        logOutPortlet.style.display = "block";
    } else {
        logInPortlet.style.display = "block";
        logOutPortlet.style.display = "none";
    }
};

// Wrapper to sending data to server
function sendData(httpMethod, url, data, callback) {
    console.log("sending data. Method: " + httpMethod + " url: " + url);
    
	var xhr = new XMLHttpRequest();
	xhr.open(httpMethod, url);
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhr.setRequestHeader("Accept", "application/json");
	
	xhr.onreadystatechange = function () {
		if (this.readyState === 4) {
			callback(this);
		}
	};
	
	if (data !== null) xhr.send(JSON.stringify(data));
	else xhr.send();
}

// Submit registration form
function registerSendData() {
	
	if (!validatePasswords()) return false;
	
	var jsonObject = new Object();
	jsonObject.userName = document.getElementById("loginInput").value;
	jsonObject.role = document.getElementById("roleSelect").value;
	jsonObject.firstName = document.getElementById("firstNameInput").value;
	jsonObject.lastName = document.getElementById("lastNameInput").value;
	jsonObject.email = document.getElementById("emailInput").value;
	jsonObject.phone = document.getElementById("phoneInput").value;
	jsonObject.address = document.getElementById("addressInput").value;
	jsonObject.password = document.getElementById("passwordInput").value;
	
	
	var callback = function(xhr) {
		if (xhr.status === 201) {
			showMessage("User " + jsonObject.userName + " successfully created");
			refreshPage();
		}
		else if (xhr.status === 409) {
			showMessage("User " + jsonObject.userName + " already exists!");
		}
		else httpError(xhr);
	};
	
	sendData("POST", 'http://localhost:8080/helpdesk/registration', jsonObject, callback);
}

function validatePasswords() {
	
	var password = document.getElementById("passwordInput");
	var password2 = document.getElementById("passwordConfirmInput");
	
	if (password.value !== password2.value) {
		password2.focus();
		showMessage("Passwords differ!");
		return false;
	}
	
	return true;
}

function registerRolesSelect() {
	var role = document.getElementById("roleSelect").value;
	var contractorDiv = document.getElementById("contractorDetails");
	
	if (role === "contractor") {
		contractorDiv.style.display = "block";
	} else {
		contractorDiv.style.display = "none";
	}
}

function chargeSystemChange() {
	var hourlyRateRadio = document.getElementById("hourlyRateRadio");
	var hourlyRateDiv = document.getElementById("hourlyRate");
	
	if (hourlyRateRadio.checked) {
		hourlyRateDiv.style.display = "block";
	} else {
		hourlyRateDiv.style.display = "none";
	}
	
}

function showMessage(message) {
	alert(message);
}

function refreshPage() {
	location.reload();
}

function logIn() {
    var loginName = document.getElementById("loginName").value;
    var loginPass = document.getElementById("loginPass").value;
    var loginRole = document.getElementById("loginRoleSelect").value;

    var jsonObject = new Object();
    jsonObject.userName = loginName;
    jsonObject.password = loginPass;
    jsonObject.role = loginRole;
    
    var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Successfully logged in");
            localStorage.loggedIn = "true";
            refreshPage();
        }
        else if (xhr.status === 403 || xhr.status === 404) {
            showMessage("Login failed!");
        }
        else httpError(xhr);
    };
    
    sendData("POST", 'http://localhost:8080/helpdesk/login', jsonObject, callback);
}

function httpError(xhr) {
    showMessage("Error occurred. Status: " + xhr.status + " Response: " + xhr.responseText);
}

function logOut() {
    var callback = function (xhr) {
        if (xhr.status === 204) {
            showMessage("Successfully logged out");
            localStorage.loggedIn = "false";
            refreshPage();
        }
        else httpError(xhr);
    };
    
    sendData("DELETE", 'http://localhost:8080/helpdesk/login', null, callback);
}
