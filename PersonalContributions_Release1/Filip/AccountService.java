/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpdesk.services;

import helpdesk.entity.Account;
import helpdesk.entity.Administrator;
import helpdesk.entity.Contractor;
import helpdesk.entity.Migrant;
import helpdesk.entity.Role;
import helpdesk.entity.Volunteer;
import helpdesk.utils.RoleUtils;
import helpdesk.webservices.errors.RestException;
import helpdesk.webservices.pojos.AccountModel;
import helpdesk.webservices.pojos.LoginModel;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response;

/**
 *
 * @author Revan
 */
@Stateless
public class AccountService {
    
    @PersistenceContext
    EntityManager em;
    
    public Role createAccount(AccountModel accountModel)
    {
        Account duplicate = findAccountByName(accountModel.getUserName());
        if (duplicate != null) throw new RestException(Response.Status.CONFLICT);
        
        Account account = createAccountBasedOnModel(accountModel);
        Role role = createRoleBasedOnModel(accountModel, account);
        
        em.persist(account);
        em.persist(role);
        
        return role;
    }
    
    private Account createAccountBasedOnModel(AccountModel accountModel)
    {
        Account account = new Account(accountModel);
        return account;
    }
    
    private Role createRoleBasedOnModel(AccountModel accountModel, Account account)
    {
        Role role;
        
        switch(accountModel.getRole()) {
            case "migrant":
                role = new Migrant(accountModel);
                break;
            case "admin":
                role = new Administrator(accountModel);
                break;
            case "contractor":
                role = new Contractor(accountModel.getContractorDetails());
                break;
            case "volunteer":
                role = new Volunteer(accountModel);
                break;
            default:
                throw new RuntimeException();
        }
        
        role.setAccount(account);
        
        return role;
    }
    
    public Account findAccountByName(String userName)
    {
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a WHERE a.userName = :name", Account.class);
        query.setParameter("name", userName);
        List<Account> results = query.getResultList();
        
        return (results.isEmpty()) ? null : results.get(0);
    }
    
    public <T extends Role> T findRoleByName(String userName, Class<T> roleClass) {
        
        String q = "select role from " + roleClass.getSimpleName() + " role join role.account acc where acc.userName = :name";
        
        TypedQuery<T> query = em.createQuery(q, roleClass);
        query.setParameter("name", userName);
        List<T> results = query.getResultList();
        
        return (results.isEmpty()) ? null : results.get(0); 
    }
    
    public void login(LoginModel login) {
        
        Account account = findAccountByName(login.getUserName());
        if (account == null) throw new RestException(Response.Status.NOT_FOUND);
        
        if (!account.getPassword().equals(login.getPassword())) throw new RestException(Response.Status.FORBIDDEN);
        
        Class<? extends Role> roleClass = RoleUtils.stringToRole(login.getRole());
        Role role = findRoleByName(account.getUserName(), roleClass);
        
        if (role == null) throw new RestException(Response.Status.FORBIDDEN);
    }
}
